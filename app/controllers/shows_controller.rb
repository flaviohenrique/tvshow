class ShowsController < ApplicationController
  def index
    render json: User.order(created_at: :desc).find_each do |u| u.show.last end
  end
end
